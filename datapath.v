// VERY IMPORTANT DISCLAIMER//////
// This code is being used as a starting point for lab 7 and it is from Mark Howarth and Michael Ji's solution for lab 6
// therefore we admit that this code baseline is a) not ours and b) being used as a starting point for this lab
// ie. all the files and modules are taken from their solution

//The Original Code taken from Partner 1 and Partner 2's Lab 5 code
//Was modified for the Lab6


module datapath (
		clk,
		//Modified parts for lab6
		mdata, //the very top input "ORIGINALLY datapath_in"
		sximm8, sximm5,//The 16bit sign extended 8bit value from the Instruction Register
		PC, // 8bit Program Counter
		vsel, //vsel is now 4bit one-hot code to select between 4 choices
		//Original part from the Lab5 code
		readnum, writenum, 
		write, loada, loadb, asel, bsel, loads, loadc,
		shift, ALUop,
		datapath_out, //output of the entire Simple RISC Machine
		status //needed for the lab5_top
		);
	
	input [15:0] mdata; //the very top input "ORIGINALLY datapath_in"
	input [15:0] sximm8;
	input [15:0] sximm5; //The 16bit sign extended 8bit value from the Instruction Register
	input [7:0] PC; // 8bit Program Counter
	input [3:0] vsel; //vsel is now 4bit one-hot code to select between 4 choices
	//Original part from the Lab5 code
	input [2:0] readnum, writenum;
	input [0:0] write, clk, loada, loadb, asel, bsel, loads, loadc;
	input [1:0] shift, ALUop;
	output [15:0] datapath_out; //output of the entire Simple RISC Machine
	output [2:0] status;
	
	wire [15:0] ALU_out;
	wire [15:0] data_in; // The output of a "Write-Back Multiplexer"
	wire [15:0] data_out; // The output of the eight register with an eightmux ANDed with 'write'
	wire [15:0] Aout,Bout; //These refer to the outputs of A and B registers
	wire [15:0] Ain, Bin; //Theses refer to the inputs into the ALU block from the input Muxes
	wire [15:0] shifterOut; //This is the output of a shifter unit
	wire [2:0] status_out; //The output of 3 bit ALU
	
	//Modified Writebackmultiplexer
	Mux4 #(16) writebackMux ( mdata, sximm8, {8'b0,PC}, datapath_out, vsel, data_in); //Write Back Multiplexer of the datapath
	//Code from Lab5
	Regfile REGFILE ( writenum , readnum, write, clk, data_in, data_out); //Working registers for the datapath
	vDFFE #(16) pipe_reg_A ( clk, loada, data_out, Aout); //Intermediate register A
	vDFFE #(16) pipe_reg_B ( clk, loadb, data_out, Bout); //Intermediate register B
	Shifter shifter ( Bout, shift, shifterOut); // Shifter Unit
	Mux2 #(16) opMuxA ( 16'b0, Aout, asel, Ain); //Source Operand Multiplexer for A
	//Modified MuxB
	Mux2 #(16) opMuxB ( sximm5, shifterOut, bsel, Bin); //Source Operand Multiplexer for B
	
	Alu alu( ALUop, Ain, Bin, ALU_out, status_out); //Arithmetic Logic Unit for the Simple RISC Machine
	
	vDFFE #(3) status_reg (clk, loads, status_out, status); //Its a register for the output of ALU "status_out" when "loads" is hight
	vDFFE #(16) reg_C (clk, loadc, ALU_out, datapath_out); //Pipeline Register for C which outputs the datapath
	
endmodule