module stage1_top_tb;
reg [15:0] read_data;
reg clk,s,reset;
wire [1:0] mem_cmd;
wire [8:0] mem_addr;
wire [8:0] write_data;

stage1_top dut(clk, reset,s, mem_cmd, mem_addr, write_data);

initial forever begin
#2;
clk = 1'b0;
#2;
clk =1'b1;
end 

initial begin
// to load the data into the register 
reset = 1'b1;
#10;
s = 1'b0;
#10;
// testing reset to the fsm
s = 1'b1;// just because im too lazy to take the s input away from the original cpu

#10;
// dependent on clock cycles
#120;

$stop;
end
endmodule 