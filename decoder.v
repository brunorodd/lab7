// VERY IMPORTANT DISCLAIMER//////
// This code is being used as a starting point for lab 7 and it is from Mark Howarth and Michael Ji's solution for lab 6
// therefore we admit that this code is a) not ours and b) being used as a starting point for this lab
// ie. all the files and modules are taken from their solution

// Binary to one-hot decoder
module Dec (a,b) ; //was taken from the slide
	parameter n = 2; // input bitwidth
	parameter m = 4; // output bitwidth
	
	input [n-1:0] a;
	output [m-1:0] b;
	
	wire [m-1:0] b = 1 << a;
endmodule