`define M_READ 2'b10
`define M_WRITE 2'b11

module stage2_top(clk, reset,s, mem_cmd, mem_addr, write_data, N, V, Z, w);
input clk, s, reset;
output [1:0] mem_cmd;
output [8:0] mem_addr;
output [15:0] write_data;
output N, V, Z, w;
wire msel, eqComp2out, eqComp3out;
wire [15:0] dout, read_data;
wire write;

/// NEW HARDWARE FOR STAGE 2
eqComp #(1) EqComp1(mem_addr[8], 1'b0, msel);
eqComp #(2) EqComp2(mem_cmd, `M_READ, eqComp2out);
eqComp #(2) EqComp3(mem_cmd,`M_WRITE, eqComp3out);

// tristate driver
assign read_data = (msel & eqComp2out) ? dout: 16'bz; 


// write assign for the input of RAM

assign write = msel & eqComp3out;
RAM #(16, 8, "data.txt") MEM(clk,mem_addr[7:0], mem_addr[7:0],write,write_data,dout);

cpu2 CPU(.read_data(read_data),.clk(clk),.reset(reset),.s(s), .mem_cmd(mem_cmd), .mem_addr(mem_addr), .write_data(write_data), .N(N), .V(V), .Z(Z), .w(w));

endmodule