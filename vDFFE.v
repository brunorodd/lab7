// VERY IMPORTANT DISCLAIMER//////
// This code is being used as a starting point for lab 7 and it is from Mark Howarth and Michael Ji's solution for lab 6
// therefore we admit that this code baseline is a) not ours and b) being used as a starting point for this lab
// ie. all the files and modules are taken from their solution
module vDFFE (clk, en, in, out) ;
	parameter n = 1;
	input clk, en;
	input [n-1:0] in;
	output [n-1:0] out ;
	reg [n-1:0] out ;
	wire [n-1:0] next_out;
	
	assign next_out = en ? in: out;
	
	always @(posedge clk)
		out = next_out;
endmodule