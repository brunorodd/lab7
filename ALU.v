// VERY IMPORTANT DISCLAIMER//////
// This code is being used as a starting point for lab 7 and it is from Mark Howarth and Michael Ji's solution for lab 6
// therefore we admit that this code baseline is a) not ours and b) being used as a starting point for this lab
// ie. all the files and modules are taken from their solution
`include "constants.v"
/*The Original Code taken from Partner 1 and Partner 2's Lab 5 code
  Was modified for the Lab6
*/

//Arithmetic Logic Unit which does 4 types of Operator 
//to the two inputs, and outputs the operated value
//ALUop of 2'b00 does the addition
//ALUop of 2'b01 does the subtraction
//ALUop of 2'b11 outputs the "not" value of one of the inputs
//ALUop of 2'b00 outputs "AND"ed value of the two inputs
//This unit is a combinational logic and not a sequntial one
module Alu ( ALUop, Ain, Bin, result, status);
	input [1:0] ALUop; //2 bit operator
	input [15:0] Ain, Bin; // 16 bit inputs
	output reg [15:0] result;
	
	//Modified Status to have 3 bit output
		//Status[0] = Zero Status 	Register bit AKA "Zero Flag"
		//Status[1] = Overflow Status Register bit AKA "Overflow Flag"
		//Status[2] = Negative Status Register bit AKA "Negative Flag"
	output [2:0] status; //Only true when Alu outputs 16bits of zeros
	
	
	//tempAddSub for the AddSub module
	wire [15:0] tempAddSub;
	
	wire sub = (ALUop == 2'b01); //If ALUop satisfies the boolean, it will make it have a single bit value of 1

	AddSub #(16) ovf(Ain,Bin, sub, tempAddSub , overflow); // gives overflow value and either subtracts or adds
	
	
	//Combinational logic which starts on any changes of all inputs to ALU to start the selected operation	
	always @(*) begin 
		
		case (ALUop)
			`ALU_ADD: result = tempAddSub;  			//addition, sub=0
			`ALU_SUB: result = tempAddSub;			//Subtraction, sub =1
			`ALU_NOT: result = ~Bin;				//Negation of Bin, sub =0 to satisfy Type 1 Always block rule
			`ALU_AND: result = (Ain & Bin);  			//ANDed values, sub=0 for Type 1 Always block rule
			default: result = 16'bxxxx_xxxx_xxxx_xxxx;  	//Default case
		endcase
	end
	
	assign status[2] = result [15]; //the most significant bit which tells the sign
	assign status[1] = overflow;
	assign status[0] = (~| result[15:0]); //NOR of all result
	
endmodule // alu





//////TAKEN FROM THE SLIDE SET 6 FROM THE LECTURE SLIDES//////
// add a+b or subtract a-b, check for overflow!
module AddSub(a,b,sub,s,ovf) ;
  parameter n = 8 ;
  input [n-1:0] a, b ;
  input sub ;           // subtract if sub=1, otherwise add
  output [n-1:0] s ;
  output ovf ;          // 1 if overflow
  wire c1, c2 ;         // carry out of last two bits
  wire ovf = c1 ^ c2 ;  // overflow if signs don't match

  // add non sign bits
  Adder1 #(n-1) ai(a[n-2:0],b[n-2:0]^{n-1{sub}},sub,c1,s[n-2:0]) ;
  // add sign bits
  Adder1 #(1)   as(a[n-1],b[n-1]^sub,c1,c2,s[n-1]) ;
endmodule

////ALSO TAKEN FROM SLIDE SET/////
module Adder1(a,b,cin,cout,s) ;
  parameter n = 8 ;
  input [n-1:0] a, b ;
  input cin ;
  output [n-1:0] s ;
  output cout ;
  wire [n-1:0] s;
  wire cout ;

  assign {cout, s} = a + b + cin ;
endmodule 
