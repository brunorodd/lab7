module Mux3new(a2,a1,a0,s, out);
parameter k=1;
input [k-1:0] a2, a1, a0 ;
input [1:0] s ;
output reg [k-1:0] out;

always @(*) begin
case(s)
2'b00: out = a0;
2'b01: out = a1;
2'b11: out = a2;
default: out = {k{1'bx}};
endcase 
end
endmodule
