`include "constants.v"
module lab7_top(KEY,SW,LEDR,HEX0,HEX1,HEX2,HEX3,HEX4,HEX5);
input [3:0] KEY;
input [9:0] SW;
output [9:0] LEDR;
output [6:0] HEX0,HEX1,HEX2,HEX3,HEX4,HEX5;

wire clk, s, reset;
wire [1:0] mem_cmd;
wire [8:0] mem_addr;
wire [15:0] dout;
wire write;
wire msel, eqComp2out, eqComp3out, N, V, Z, w;
wire [15:0] read_data, write_data;
wire [9:0] SW;
wire Totristate,Totristate2;
wire ToVDFFE,ToVDFFE2;




/// NEW HARDWARE FOR STAGE 2
eqComp #(1) EqComp1(mem_addr[8], 1'b0, msel);
eqComp #(2) EqComp2(mem_cmd, `M_READ, eqComp2out);
eqComp #(2) EqComp3(mem_cmd,`M_WRITE, eqComp3out);

// tristate driver
assign read_data = (msel & eqComp2out) ? dout: 16'bz; 


// write assign for the input of RAM

assign write = msel & eqComp3out;
RAM #(16, 8, "data.txt") MEM(~KEY[0],mem_addr[7:0], mem_addr[7:0],write,write_data,dout);

cpu CPU(.read_data(read_data),.clk(~KEY[0]),.reset(~KEY[1]),.s(~KEY[2]), .mem_cmd(mem_cmd), .mem_addr(mem_addr), .write_data(write_data), .N(N), .V(V), .Z(Z), .w(w));



//*//stage 3//*//
//left circuit into triState
eqComp #(9) circuitToSW(mem_addr, 9'h140, Totristate);
eqComp #(2) circuitToSW2(mem_cmd, `M_READ, Totristate2);

assign read_data[7:0] = (Totristate&Totristate2) ? SW[7:0] : 8'bz;

//right circuit into vDFFE
eqComp #(9) circuitToLED(mem_addr, 9'h100, ToVDFFE);
eqComp #(2) circuitToLED2(mem_cmd, `M_WRITE, ToVDFFE2);


vDFFE #(8) LED( ~KEY[0], (ToVDFFE & ToVDFFE2), write_data[7:0], LEDR[7:0]);



//(from lab 6)
 // fill in sseg to display 4-bits in hexidecimal 0,1,2...9,A,B,C,D,E,F
  sseg H0(write_data[3:0],   HEX0);
  sseg H1(write_data[7:4],   HEX1);
  sseg H2(write_data[11:8],  HEX2);
  sseg H3(write_data[15:12], HEX3);
/* status signals? */
  assign HEX4 = 7'b1111111;
  assign {HEX5[2:1],HEX5[5:4]} = 4'b1111; // disabled
   

  assign HEX5[0] = ~Z;
  assign HEX5[6] = ~N;
  assign HEX5[3] = ~V;
 
endmodule








// The sseg module below can be used to display the value of datpath_out on
// the hex LEDS the input is a 4-bit value representing numbers between 0 and
// 15 the output is a 7-bit value that will print a hexadecimal digit.  You
// may want to look at the code in Figure 7.20 and 7.21 in Dally but note this
// code will not work with the DE1-SoC because the order of segments used in
// the book is not the same as on the DE1-SoC (see comments below).

// Same additions as used for lab5_top
module sseg(in,segs);
  input [3:0] in;
  output [6:0] segs;
  reg [6:0] segs;
  
  // NOTE: The code for sseg below is not complete: You can use your code from
  // Lab4 to fill this in or code from someone else's Lab4.  
  //
  // IMPORTANT:  If you *do* use someone else's Lab4 code for the seven
  // segment display you *need* to state the following three things in
  // a file README.txt that you submit with handin along with this code: 
  //
  //   1.  First and last name of student providing code
  //   2.  Student number of student providing code
  //   3.  Date and time that student provided you their code
  //
  // You must also (obviously!) have the other student's permission to use
  // their code.
  //
  // To do otherwise is considered plagiarism.
  //
  // One bit per segment. On the DE1-SoC a HEX segment is illuminated when
  // the input bit is 0. Bits 6543210 correspond to:
  //
  //    0000
  //   5    1
  //   5    1
  //    6666
  //   4    2
  //   4    2
  //    3333
  //
  // Decimal value | Hexadecimal symbol to render on (one) HEX display
  //             0 | 0
  //             1 | 1
  //             2 | 2
  //             3 | 3
  //             4 | 4
  //             5 | 5
  //             6 | 6
  //             7 | 7
  //             8 | 8
  //             9 | 9
  //            10 | A
  //            11 | b
  //            12 | C
  //            13 | d
  //            14 | E
  //            15 | F
  //assign segs = 7'b0001110;  // this will output "F" 
  always @(in) begin
    case (in)
      4'd0: segs = 7'b1000000; //Prints "0"
      4'd1: segs = 7'b1111001; //Prints "1"
      4'd2: segs = 7'b0100100; //Prints "2"
      4'd3: segs = 7'b0110000; //Prints "3"
      4'd4: segs = 7'b0011001; //Prints "4"
      4'd5: segs = 7'b0010010; //Prints "5"
      4'd6: segs = 7'b0000010; //Prints "6"
      4'd7: segs = 7'b1011000; //Prints "7"
      4'd8: segs = 7'b0000000; //Prints "8"
      4'd9: segs = 7'b0010000; //Prints "9"
      4'd10: segs = 7'b0001000; //Prints "A"
      4'd11: segs = 7'b0000011; //Prints "b"
      4'd12: segs = 7'b1000110; //Prints "C"
      4'd13: segs = 7'b0100001; //Prints "d"
      4'd14: segs = 7'b0000110; //Prints "E"
      4'd15: segs = 7'b0001110; //Prints "F"
      //default: segs = 7'bxxxxxxx;
      endcase
    end

endmodule