// VERY IMPORTANT DISCLAIMER//////
// This code is being used as a starting point for lab 7 and it is from Mark Howarth and Michael Ji's solution for lab 6
// therefore we admit that this code baseline is a) not ours and b) being used as a starting point for this lab
// ie. all the files and modules are taken from their solution

//The Original Code taken from Partner 1 and Partner 2's Lab 5 code
//Was modified for the Lab6

`include "constants.v"

module Shifter ( in, shift, out);
	input [15:0] in;
	input [1:0] shift;
	output [15:0] out;
	reg [15:0] out;



	always @(*) begin
	case (shift)
	`SH_NONE: out = in;
	`SH_LSL: out = {in[14:0], 1'b0};	//shift it to left
	`SH_LSR: out = {1'b0, in[15:1]};	//shift it to right
	`SH_ASR: out = {in[0], in[15:1]};  //shift it to right, last one becomes the first one
	default: out = 16'bxxxx_xxxx_xxxx_xxxx;
	endcase
	end

endmodule