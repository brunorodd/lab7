// VERY IMPORTANT DISCLAIMER//////
// This code is being used as a starting point for lab 7 and it is from Mark Howarth and Michael Ji's solution for lab 6
// therefore we admit that this code baseline is a) not ours and b) being used as a starting point for this lab
// ie. all the files and modules are taken from their solution
`include "constants.v"
module Mux2 ( a1, a0, s, out) ; //modified from the regfile.v
	parameter k = 1;
	input [k-1:0]  a1, a0 ;
	input [0:0] s ; // one bit code
	output [k-1:0] out;
	wire [k-1:0] out = 	({k{s}} & a1) |
				({k{~s}} & a0) ;
							
endmodule

module Mux3( a2, a1, a0, s, out);
	parameter k = 1;
	input [k-1:0] a2, a1, a0 ;
	input [2:0] s ;
	output [k-1:0] out;
	wire [k-1:0] out = 	({k{s[0]}} & a0) |
				({k{s[1]}} & a1) |
				({k{s[2]}} & a2);
endmodule

//MUX4 for the Modified WriteBackMultiplexer
module Mux4 ( a3, a2, a1, a0, s, out);
	parameter k = 1;
	input [k-1:0] a3, a2, a1, a0;
	input [3:0] s;
	output [k-1:0] out;
	wire [k-1:0] out = 	({k{s[3]}} & a3) |
				({k{s[2]}} & a2) |
				({k{s[1]}} & a1) |
				({k{s[0]}} & a0) ;
							
endmodule

// 8 input multiplexer with one-hot select input
module Mux8 ( a7, a6, a5, a4, a3, a2, a1, a0, s, out) ; //also taken from slide but modified for 8 types of register
	parameter k = 1;
	input [k-1:0] a7, a6, a5, a4, a3, a2, a1, a0 ;
	input [7:0] s ; // one hot "read-num" number
	output [k-1:0] out;
	wire [k-1:0] out = 	({k{s[0]}} & a0) |
				({k{s[1]}} & a1) |
				({k{s[2]}} & a2) |
				({k{s[3]}} & a3) |
				({k{s[4]}} & a4) |
				({k{s[5]}} & a5) |
				({k{s[6]}} & a6) |
				({k{s[7]}} & a7) ;
							
	
endmodule