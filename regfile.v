// VERY IMPORTANT DISCLAIMER//////
// This code is being used as a starting point for lab 7 and it is from Mark Howarth and Michael Ji's solution for lab 6
// therefore we admit that this code baseline is a) not ours and b) being used as a starting point for this lab
// ie. all the files and modules are taken from their solution

//The Original Code taken from Partner 1 and Partner 2's Lab 5 code
//Was modified for the Lab6


/* 
   8 register regfile containing 8 * 16-bit
   Uses 3-bit binary codes for writenum and readnum to specify which register
   is read / written. Rising edge activated on clk. Data_in will be stored
   in register addressed by readnum when 'write' is HIGH and clk has a rising
   edge.
 */
module Regfile ( writenum , readnum, write, clk, data_in, data_out);
   // Binary code address of registers
	input [2:0] writenum, readnum;
   // When HIGH, allows values to be written from data_in into registers
	input write;
   // The clock :)
   input clk;
   
   // 16-bit data input that can be stored if wanted into registers
	input [15:0] data_in;
   
   // 16-bit data output driven by the register value specified by readnum
	output [15:0] data_out;
	wire [15:0] data_out;
   
   // decoded one-hot codes for use with one-hot multiplexers
	wire [7:0] writeEight, readEight; 
   
   // decoded one-hot codes anded with write to enable write on DFFE's
	wire [7:0] deciWrite;
   
   // register values driven by register flip-flops
	wire [15:0] R0,R1,R2,R3,R4,R5,R6,R7;
	
   // decoder from writenum to one-hot code for writing
	Dec #(3,8) writing(writenum, writeEight); //decoder for writing
   // decoder from readnum to one-hot code for reading
	Dec #(3,8) reading(readnum, readEight); //decoder for reading
	
	// ANDing all the writenum one-hot code with write to see which register to write to
   assign deciWrite = ({8{write}} & writeEight);
	
	vDFFE #(16) register0 (clk, deciWrite[0], data_in, R0); //each eight registers
	vDFFE #(16) register1 (clk, deciWrite[1], data_in, R1);
	vDFFE #(16) register2 (clk, deciWrite[2], data_in, R2);
	vDFFE #(16) register3 (clk, deciWrite[3], data_in, R3);
	vDFFE #(16) register4 (clk, deciWrite[4], data_in, R4);
	vDFFE #(16) register5 (clk, deciWrite[5], data_in, R5);
	vDFFE #(16) register6 (clk, deciWrite[6], data_in, R6);
	vDFFE #(16) register7 (clk, deciWrite[7], data_in, R7);
	
	//Upon each ReadEight one-hot code, mux uses it for one of the registers and puts out "data_out"
	Mux8 #(16) read_mux ( R7, R6, R5, R4, R3, R2, R1, R0, readEight, data_out);
endmodule