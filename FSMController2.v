// VERY IMPORTANT DISCLAIMER//////
// This code is being used as a starting point for lab 7 and it is from Mark Howarth and Michael Ji's solution for lab 6
// therefore we admit that this code baseline is a) not ours and b) being used as a starting point for this lab
// ie. all the files and modules are taken from their solution
`include "constants.v"
`define S_WRITE_MEM2 4'b1111
module control (
	// INPUTS
	clk,    // Clock
	reset, // reset
	opcode, 
	op, s,
	// OUTPUTS
	loada, loadb, loadc, loads,
	vsel, asel, bsel, nsel,
	write,
	w, load_ir, load_pc, reset_pc, addr_sel, mem_cmd, load_addr
);
	input clk, reset;
	input s;
	input [2:0] opcode;
	input [1:0] op;
	
	output loada, loadb, loadc, loads, reset_pc, load_pc, addr_sel, load_ir, load_addr;
	output [1:0] mem_cmd;
	output asel, bsel;
	output [3:0] vsel; // one hot
	output [2:0] nsel; // one hot
	output write;
	output w;
	
	 
	
	
	// output width
	`define OUTW 21
   `define La 3'b100 // load a
   `define Lb 3'b010 // load b
   `define Lc 3'b001 // load c
   `define Lnone 3'b000 // load none
	
	//////////STATE MACHINE//////////////
	// From slide set 5
	wire [`SW-1:0] present_state, next_state_reset, next_state;
	reg  [(`SW + `OUTW+1)-1:0] next;
	
	vDFF #(`SW) STATE(clk,next_state_reset,present_state);
	
	// reset logic
	assign next_state_reset = reset ? `S_RESET : next_state;
	
	///// States /////
	always @(*) begin
		casex( {present_state, s, opcode, op})                       // these are not the right size
			{`S_RESET, 1'bx,   3'bxxx, 2'bxx}: next = {`S_IF1, {(`OUTW-6){1'b0}},4'b0110, `M_NONE, 1'b0}; // reset -> IF1 //GETS PAST THIS
      	  		{`S_IF1, 1'bx, 3'bxxx, 2'bxx}: next = {`S_IF2, {(`OUTW-6){1'b0}}, 4'b0001,`M_READ, 1'b0};//IF1 -> IF2 // STAGE 1 GETS STUCK HERE FOR SOME REASON//////
			{`S_IF2, 1'bx, 3'bxxx, 2'bxx}: next = {`S_UPDATEPC, {(`OUTW-6){1'b0}}, 4'b1001,`M_READ, 1'b0};//IF2-> UPDATEPC
			{`S_UPDATEPC, 1'bx, 3'bxxx, 2'bxx}: next = {`S_DECODE, {(`OUTW-6){1'b0}},4'b0100, `M_NONE, 1'b0};//UPDATEPC -> DECODE
      // --- Decode stage --- //   

			{`S_DECODE, 1'bx, `OP_MOV, `MOV_LOAD}: next = {`S_WRITE_IMM,{(`OUTW-2){1'b0}}, `M_NONE,1'b0}; //decode -> writeImm
         {`S_DECODE, 1'bx, `OP_MOV, `MOV_MOVE}: next = {`S_GET_B,{`OUTW{1'b0}}, 1'b0}; //decode -> getB -> doMath (asel = 1) -> writeReg
	 {`S_DECODE, 1'bx, `OP_LDR, `ALU_ADD} : next = {`S_GET_A, `Lnone,1'b0, `VS_NONE, 2'b0, `Rnone, 2'b0, 4'b0001, `M_NONE, 1'b0}; //decode ->GET_A (LDR)
	 {`S_DECODE, 1'bx, `OP_STR, `ALU_ADD} : next = {`S_GET_A, `Lnone,1'b0, `VS_NONE, 2'b0, `Rnone, 2'b0, 4'b0001, `M_NONE, 1'b0}; //decode ->GET_A (STR)
	


         // ALU decoding
         {`S_DECODE, 1'bx, `OP_ALU, `ALU_ADD}: next = {`S_GET_A, {`OUTW{1'b0}}, 1'b0}; // ADD
         {`S_DECODE, 1'bx, `OP_ALU, `ALU_SUB}: next = {`S_GET_A, {`OUTW{1'b0}}, 1'b0}; // CMP
         {`S_DECODE, 1'bx, `OP_ALU, `ALU_AND}: next = {`S_GET_A, {`OUTW{1'b0}}, 1'b0}; // AND
         {`S_DECODE, 1'bx, `OP_ALU, `ALU_NOT}: next = {`S_GET_B, {`OUTW{1'b0}}, 1'b0}; // MVN

// LDR and STR and HALT ops	(Stage 2)
							////////_______ new values accounting for the new outputs of the FSM/////
	//LDR: GET_Rn -> ADD_SXIMM5 -> STORE_DATA -> READ_MEM
	 {`S_GET_A, 1'bx, `OP_LDR, 2'bxx} : next = {`S_DO_MATH, `La, 1'b0, `VS_CREG, 2'b00,`Rn, 1'b0, 1'b0, 1'b0, 2'b00, 1'b0, 2'b01,1'b0}; // getRn -> Add
	 {`S_DO_MATH, 1'bx, `OP_LDR, `ALU_ADD} : next = {`S_STORE_DATA, `Lc, 1'b0, `VS_CREG, 1'b0, 1'b1/*bsel = 1*/, `Rn, 1'b0, 1'b0,1'b0, 1'b0, 1'b0, 1'b0, 2'b01, 1'b0};
	 {`S_STORE_DATA, 1'bx, `OP_LDR, 2'bxx} : next = {`S_READ_MEM, `Lnone, 1'b0, `VS_NONE, 2'b0, `Rn, 1'b0, 1'b0, 2'b0, 1'b0, 1'b0, 2'b01, 1'b1/*load addr = 1 b/c saving to data address*/};
	
	 {`S_READ_MEM, 1'bx, `OP_LDR, 2'bxx} : next = {`S_WRITE_REG, `Lnone, 1'b0, `VS_NONE, 2'b0, `Rn,1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, `M_READ, 1'b0}; //value read from mem should be saved in R[Rd]
	 {`S_WRITE_REG, 1'bx, `OP_LDR, 2'bxx} : next = {`S_IF1,`Lc, 1'b0, `VS_MDATA, 2'b0, `Rd, 1'b1, 1'b0, 4'b0, `M_READ, 1'b0};

	//STR: GET_Rn -> ADD_SXIMM5 -> STORE_DATA -> DATAOUT_STR (output to write_data) -> WRITE_MEM
	 {`S_GET_A, 1'bx, `OP_STR, 2'bxx} : next = {`S_DO_MATH, `La, 1'b0, `VS_CREG, 2'b00,`Rn, 2'b00, 2'b00, 1'b0, 1'b0, `M_NONE ,1'b0}; // getRn -> Add	   
	 {`S_DO_MATH, 1'bx, `OP_STR, `ALU_ADD} : next = {`S_STORE_DATA, `Lc, 1'b0, `VS_CREG, 1'b0, 1'b1/*bsel = 1*/, `Rn, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, `M_NONE, 1'b0};
	 {`S_STORE_DATA, 1'bx, `OP_STR, 2'bxx} :next = {`S_DATAOUT_STR, `Lnone, 1'b0, `VS_CREG, 2'b00, `Rn, 1'b0, 1'b0, 1'b0, 2'b0, 1'b0, `M_NONE, 1'b1};

	 {`S_DATAOUT_STR, 1'bx, `OP_STR, 2'bxx} : next = {`S_WRITE_MEM, `Lb, 1'b0, `VS_CREG, 2'b00, `Rd, 1'b0, 1'b0, 1'b0, 2'b0, 1'b0, `M_NONE, 1'b0};
	 {`S_WRITE_MEM, 1'bx, `OP_STR, 2'bxx} : next = {`S_WRITE_MEM2 , `Lc, 1'b0, `VS_CREG, 1'b1, 1'b0, `Rn, 1'b0, 1'b0, 1'b0, 2'b0, 1'b0, `M_NONE, 1'b0};
	 {`S_WRITE_MEM2 , 1'bx,`OP_STR, 2'bxx} : next =  {`S_IF1, `Lnone, 1'b0, `VS_CREG, 1'b0,1'b0, `Rn, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, `M_WRITE, 1'b0};

	 {`S_HALT, 6'bxxx_xxx}      : next = {`S_HALT, {`OUTW{1'b0}}}; //Halt stage stays in same stage until reset is activated
         
         
      // --- MOV ops --- //
			{`S_WRITE_IMM, 1'bx, 3'bxxx,2'bxx}: next = {`S_IF1, `La, 1'b0,  `VS_SXIMM8, 1'b0, 1'b1, `Rn, 1'b1, 1'b0, 4'b0, `M_NONE, 1'b0}; // writeImm -> wait
         
         // Variation on doMath state used for MOV move operation (asel = 1), continues normal datapath
         {`S_DO_MATH, 1'bx, `OP_MOV, 2'bxx}: next = {`S_WRITE_REG, `Lc, 1'b0, `VS_NONE, 1'b1, 1'b0,`Rnone, 2'b0, 7'b0}; // doMath -> writeReg
      
      // --- ALU ops --- //
         // getA -> getB -> doMath -> writeReg
         {`S_GET_A, 6'bxxx_xxx} : next = {`S_GET_B, `La, 1'b0, `VS_NONE, 2'b0,`Rn, 2'b0, 7'b0}; // getA -? getB
         {`S_GET_B, 6'bxxx_xxx} : next = {`S_DO_MATH, `Lb, 1'b0, `VS_NONE, 2'b0,`Rm, 2'b0, 7'b0}; // getB -? doMath
         
         // doMath CMP
         {`S_DO_MATH, 1'bx, `OP_ALU, `ALU_SUB}: next = {`S_RESET, `Lnone, 1'b1, `VS_NONE, 1'b0, 1'b0,`Rnone, 2'b0, 7'b0}; // CMP
         
	 // normal doMath state
         {`S_DO_MATH, 6'bx_xxx_xx}: next = {`S_WRITE_REG, `Lc, 1'b0, `VS_NONE, 1'b0, 1'b0,`Rnone, 2'b0, 7'b0};
         
         // writeReg
         {`S_WRITE_REG, 6'bx_xxx_xx}: next = {`S_IF1, `Lnone, 1'b0, `VS_CREG, 2'b0, `Rd, 1'b1, 1'b0, 7'b0};
			
			default: next = {{`SW{1'bx}},{`OUTW{1'bx}}};
		endcase
	end	
 	
	assign {next_state, loada, loadb, loadc, loads,	vsel, asel, bsel, nsel, write, w, load_ir, load_pc, reset_pc, addr_sel, mem_cmd, load_addr} = next;

endmodule