// This code is being used as a starting point for lab 7 and it is from Mark Howarth and Michael Ji's solution for lab 6
// therefore we admit that this code baseline is a) not ours and b) being used as a starting point for this lab
// ie. all the files and modules are taken from their solution

/* coding a state machine shown in Slide Set 5 (using a separate
   vDFF module for the state, an assign statement for the reset mux, and a separate always block for your
   combinational logic for state transition logic and output logic). */

`include "constants.v"

/*
 * Structural CPU module. Primarily connects controller FSM implemented
 * by 'control' module with datapath. Decodes assembly instructions on in
 * to drive values fed to FSM and datapath. 
 */

// replaced in with read_data as an input to the cpu in accordance with the diagram
module cpu2(read_data,clk,reset,s,write_data,N,V,Z,w, mem_cmd, mem_addr);
   input clk, reset, s;
   input [15:0] read_data;
   output [15:0] write_data;
   output [8:0] mem_addr;
   output [1:0] mem_cmd;
   output N, V, Z, w;

   // Values used for later labs. zero for now
   wire [15:0] mdata = 16'b0; // For later
   wire [7:0] PC = 8'b0;
   
   // Instruction decoder driven values
   wire [2:0] opcode;
   wire [1:0] op;
   wire [2:0] Rd, Rn, Rm;
   wire [1:0] shift;
   wire [7:0] imm8;
   wire [4:0] imm5;
   wire [1:0] ALUop;
   
   // Control FSM driven values
   wire write;
   wire loada, loadb, loadc, loads;
   wire asel, bsel; 
   wire [2:0] nsel; // One-hot FSM driven select between Rn, Rm and Rd
   wire [3:0] vsel; // datapath input select one-hot code
   
   // 16 bit instruction register value
 
	wire [15:0] i;
   
   // Mux output from nsel -> readnum and writenum addresses 
   wire [2:0] rwnum;
   
    // sign extended immediates
   wire [15:0] sximm8;
   wire [15:0] sximm5;
   
// NEW WIRES FOR LAB 7
wire load_pc, reset_pc, load_ir, addr_sel,load_addr;
wire [8:0] out_PC, next_pc, out_add, dataAddrOut;


   // status flags extracted from datapath ALU results
   // (N)egative, o(V)erflow, (Z)ero
   wire [2:0] status;
   assign {N, V, Z} = status[2:0];
   
   // instruction register
   vDFFE #(16) ireg (clk,load_ir, read_data, i); // Value of read_data is only copied to our instruction register on the rising edge of clk, and at load hight
   
   /* INSTRUCTION DECODER */
   assign {opcode, op, Rm, Rd, Rn, shift, imm8, imm5, ALUop}={
      // opcode, op,
      i[15:13], i[12:11], 
      // Rm,  Rd,    Rn
      i[2:0], i[7:5], i[10:8],
      // shift
       i[4:3],
       // imm8, imm5
       i[7:0], i[4:0],
       // ALUop
       i[12:11]
   };
      
  
   
   // Mux for readnum and writenum driven by select from control FSM
   Mux3 #(3) addressMux(Rn, Rd, Rm, nsel, rwnum);
   
   //Sign Extension of the Immediates
   sx #(5,16) five (imm5, sximm5);
   sx #(8,16) eight (imm8, sximm8);
   
   // Our control FSM for the datapath
   control2 control (
      // INPUTS
      .clk(clk),    // Clock
      .reset(reset), // reset
      .opcode(opcode), 
      .op(op), .s(s),
      // OUTPUTS
      .loada(loada), .loadb(loadb), .loadc(loadc), .loads(loads),
      .vsel(vsel), .asel(asel), .bsel(bsel), .nsel(nsel),
      .write(write),
      .w(w),
// new outputs for lab 7

// NOTE: WE USED NAMED ASSOCIATION B/C THIS FILE WAS ALREADY USING IT SO THERE IS NO INTENDED COPYING
 .load_ir(load_ir), .load_pc(load_pc), .reset_pc(reset_pc), .addr_sel(addr_sel), .mem_cmd(mem_cmd)

   );
Mux2 #(9) muxPC(9'b000000000, out_add, reset_pc, next_pc);

// register for the program counter
vDFFE #(9) programCounter(clk, load_pc, next_pc, out_PC);

// register for the Data Address 
vDFFE dataAddress(clk,load_addr, write_data[8:0], dataAddrOut);


// this assigns the bus out_add to out_PC +1 b/c of the combinational logic block before the multiplexer
assign out_add = out_PC+1;
   
// MULTIPLEXER FOR THE OUTPUT (mem_addr) OF CPU
Mux2 #(9) muxOutcpu(out_PC, dataAddrOut, addr_sel, mem_addr);
 
   /*DATAPATH FROM LAB5 with lab6 modifications*/  
   datapath DP(
      .clk(clk),
      .mdata(read_data),
      .sximm8(sximm8), .sximm5(sximm5),//The 16bit sign extended 8bit value from the Instruction Register
      .PC(PC), // 8bit Program Counter
      .vsel(vsel), //vsel is now 4bit one-hot code to select between 4 choices
      .readnum(rwnum), .writenum(rwnum), 
      .write(write), .loada(loada), .loadb(loadb), .asel(asel), .bsel(bsel),
      .loads(loads), .loadc(loadc),
      .shift(shift), .ALUop(ALUop),
      .datapath_out(write_data), //output of the entire Simple RISC Machine
      .status(status) //needed for the lab5_top
   );
   
endmodule

// sign extend number
// sign extends input of with #n to output of width #m using the most
// significant bit of the input
module sx (in, out);
   parameter n = 8; // input width
   parameter m = 16; // output width
   input [n-1:0] in;
   output [m-1:0] out;
   
   assign out = { ( {(m-n){in[n-1]}} ), in[n-1:0] };

endmodule