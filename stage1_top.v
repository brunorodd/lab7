`define M_READ 2'b10
module stage1_top(clk, reset,s, mem_cmd, mem_addr);
input clk, s, reset;
output [1:0] mem_cmd;
output [8:0] mem_addr;
wire msel, eqComp2out;
wire [15:0] dout, read_data;

eqComp #(1) EqComp1(mem_addr[8], 1'b0, msel);
eqComp #(2) EqComp2(mem_cmd, `M_READ, eqComp2out);

// tristate driver
assign read_data = (msel & eqComp2out) ? dout: 16'bz; 

RAM MEM(clk,mem_addr[7:0], 8'b00000000,1'b0,16'b0,dout);

cpu1 CPU(.read_data(read_data),.clk(clk),.reset(reset),.s(s), .mem_cmd(mem_cmd), .mem_addr(mem_addr));

endmodule
