// VERY IMPORTANT DISCLAIMER//////
// This code is being used as a starting point for lab 7 and 8 and it is from Mark Howarth and Michael Ji's solution for lab 6
// therefore we admit that this code baseline is a) not ours and b) being used as a starting point for this lab
// ie. all the files and modules are taken from their solution
// MEM_CMD OUTPUTS
`define M_READ 2'b10
`define M_NONE 2'b01
`define M_WRITE 2'b11

// new outputs for lab7
`define OP_STR 3'b100
`define OP_LDR 3'b011
`define OP_HALT 3'b111
`define OP_B 	3'b001
`define OP_B2   3'b010

// opcodes for cpu
`define OP_MOV 3'b110
`define OP_ALU 3'b101
`define OP_B   3'b001

// MOVE ops
`define MOV_LOAD 2'b10
`define MOV_MOVE 2'b00

// ALU ops
`define ALU_ADD 2'b00
`define ALU_SUB 2'b01
`define ALU_AND 2'b10
`define ALU_NOT 2'b11

`define SH_NONE 2'b00
`define SH_LSL 2'b01
`define SH_LSR 2'b10
`define SH_ASR 2'b11

// --- nsel --- ///readnum / writenum mux select
`define Rn		3'b100
`define Rd		3'b010
`define Rm		3'b001
`define Rnone	3'b000

// vsel multiplexer selectors
`define VS_MDATA 	4'b1000
`define VS_SXIMM8	4'b0100
`define VS_PC		4'b0010
`define VS_CREG		4'b0001 // writeback
`define VS_NONE		4'b0000 //No value

// States
`define SW		5
`define S_RESET		5'b00000
`define S_IF1		5'b00001
`define S_IF2		5'b00010
`define S_UPDATEPC	5'b00011
`define S_DECODE	5'b00100
`define S_GET_A		5'b00101
`define S_GET_B		5'b00110
`define S_STORE_DATA    5'b00111
`define S_READ_MEM 	5'b01000
`define S_DATAOUT_STR   5'b01001
`define S_WRITE_MEM     5'b01010
`define S_HALT      	5'b01011
// new lab 8 states
`define S_BRANCH 	5'b10000
`define S_BRANCH2	5'b10001



// `define S_MOV_ADD   3'b100
`define S_DO_MATH	5'b01100
`define S_WRITE_REG	5'b01101
`define S_WRITE_IMM	5'b01110	

`define R0 3'd0
`define R1 3'd1
`define R2 3'd2
`define R3 3'd3
`define R4 3'd4
`define R5 3'd5
`define R6 3'd6
`define R7 3'd7

// new cond inputs 
`define COND_B		3'b000
`define COND_BEQ	3'b001
`define COND_BNE 	3'b010
`define COND_BLT	3'b011
`define COND_BLE	3'b100